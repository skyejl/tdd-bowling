import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BowlingTest {
    private Bowling bowling;

    @BeforeEach
    void setUp() {
        bowling = new Bowling();
    }

    @Test
    void should_get_0_when_no_throw() {
        Integer expected = 0;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_point_when_1_throw() {
        bowling.addFrames(Arrays.asList(new Frame(3)));
        Integer expected = 3;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_point_when_2_throws() {
        bowling.addFrames(Arrays.asList(new Frame(3, 4)));
        Integer expected = 7;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_point_when_10_frames_without_strike_and_spare() {
        bowling.addFrames(Arrays.asList(new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4)));
        Integer expected = 70;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_first_frame_spare() {
        bowling.addFrames(Arrays.asList(new Frame(6,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4)));
        Integer expected = 76;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_spare() {
        bowling.addFrames(Arrays.asList(new Frame(6,4), new Frame(3,4), new Frame(6,4), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4)));
        Integer expected = 82;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_last_frame_spare() {
        bowling.addFrames(Arrays.asList(new Frame(6,4), new Frame(3,4), new Frame(6,4), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(6,4), new Frame(1)));
        Integer expected = 86;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_first_strike() {
        bowling.addFrames(Arrays.asList(new Frame(10), new Frame(3,4), new Frame(6,4), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(3,4), new Frame(6,4), new Frame(1)));
        Integer expected = 90;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_strike_and_last_not_strike() {
        bowling.addFrames(Arrays.asList(new Frame(10), new Frame(3,4), new Frame(10), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(4,4), new Frame(3,4), new Frame(3,4), new Frame(6,4), new Frame(1)));
        Integer expected = 95;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

    @Test
    void should_get_total_score_when_10_frames_with_last_strike() {
        bowling.addFrames(Arrays.asList(new Frame(10), new Frame(3,4), new Frame(10), new Frame(3,4), new Frame(3,4),
                new Frame(3,4), new Frame(4,4), new Frame(3,4), new Frame(3,4), new Frame(10), new Frame(1,5)));
        Integer expected = 100;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }
    @Test
    void should_get_total_score_when_10_frames_with_all_strike() {
        bowling.addFrames(Arrays.asList(new Frame(10), new Frame(10), new Frame(10), new Frame(10), new Frame(10),
                new Frame(10), new Frame(10), new Frame(10), new Frame(10), new Frame(10), new Frame(10), new Frame(10)));
        Integer expected = 300;
        Integer score = bowling.calculateScore();
        assertEquals(expected, score);
    }

}
